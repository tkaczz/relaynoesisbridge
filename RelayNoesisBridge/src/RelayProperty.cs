﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using RelayNoesisBridge.Observable;

namespace RelayNoesisBridge {
    /// <summary>
    /// Read-write generic property, source <-> target
    /// </summary>
    public class RelayProperty<T> : ObservableProperty<T>, INotifyPropertyChanged, IDisposable {
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly PropertyChangedEventArgs propertyChangedEventArgs;
        private bool disposed;
        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool dispose) {
            if (!disposed) {
                Changed -= RelayProperty_Changed;
                this.disposed = true;
            }
        }

        private void RelayProperty_Changed(T obj) {
            PropertyChanged?.Invoke(this, propertyChangedEventArgs);
        }

        public RelayProperty(
            string name,
            T value = default(T),
            bool distinct = true,
            EqualityComparer<T> comparer = null)
        : base(value, distinct, comparer) {
            this.propertyChangedEventArgs = new PropertyChangedEventArgs(name);
            base.Changed += RelayProperty_Changed;
        }

        public RelayProperty(
            PropertyChangedEventArgs eventArgs,
            PropertyChangedEventHandler eventHandler,
            T value = default(T),
            bool distinct = true,
            EqualityComparer<T> comparer = null)
        : base(value, distinct, comparer) {
            this.propertyChangedEventArgs = eventArgs;
            this.PropertyChanged = eventHandler;
        }
    }
}