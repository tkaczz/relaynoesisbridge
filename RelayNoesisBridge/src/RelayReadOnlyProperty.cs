﻿using System;
using System.ComponentModel;
using RelayNoesisBridge.Observable;

namespace RelayNoesisBridge {
    /// <summary>
    /// Readonly property, source -> target
    /// </summary>
    public class RelayReadOnlyProperty<T> : ObservableReadOnlyProperty<T>, INotifyPropertyChanged, IDisposable {
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly PropertyChangedEventArgs propertyChangedEventArgs;
        private bool disposed;

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool dispose) {
            if (!disposed) {
                Changed -= RelayReadOnlyProperty_Changed;
                this.disposed = true;
            }
        }

        private void RelayReadOnlyProperty_Changed(T obj) {
            PropertyChanged?.Invoke(this, propertyChangedEventArgs);
        }

        public RelayReadOnlyProperty(ObservableReadOnlyProperty<T> property, string name) : base(property) {
            this.propertyChangedEventArgs = new PropertyChangedEventArgs(name);
            base.Changed += RelayReadOnlyProperty_Changed;
        }

        public RelayReadOnlyProperty(
            ObservableReadOnlyProperty<T> property,
            PropertyChangedEventArgs eventArgs,
            PropertyChangedEventHandler eventHandler
        ) : base(property) {
            this.propertyChangedEventArgs = eventArgs;
            this.PropertyChanged = eventHandler;
            base.Changed += RelayReadOnlyProperty_Changed;
        }

        public RelayReadOnlyProperty(ObservableProperty<T> property, string name) : base(property) {
            this.propertyChangedEventArgs = new PropertyChangedEventArgs(name);
            base.Changed += RelayReadOnlyProperty_Changed;
        }

        public RelayReadOnlyProperty(
            ObservableProperty<T> property,
            PropertyChangedEventArgs eventArgs,
            PropertyChangedEventHandler eventHandler)
        : base(property) {
            this.propertyChangedEventArgs = eventArgs;
            this.PropertyChanged = eventHandler;
            base.Changed += RelayReadOnlyProperty_Changed;
        }
    }
}