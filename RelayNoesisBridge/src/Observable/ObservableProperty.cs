﻿using System;
using System.Collections.Generic;
using RelayNoesisBridge.Observable.Interfaces;

namespace RelayNoesisBridge.Observable {
    public class ObservableProperty<T> : IObservableProperty<T> {
        private readonly bool distinct;
        private readonly EqualityComparer<T> comparer;

        private T current;
        public T Value {
            get { return current; }

            set {
                if (distinct) {
                    if (!comparer.Equals(current, value)) {
                        current = value;
                        Changed?.Invoke(current);
                    }
                }
                else {
                    current = value;
                    Changed?.Invoke(current);
                }
            }
        }

        public event Action<T> Changed;

        public ObservableProperty(T value = default(T), bool distinct = true, EqualityComparer<T> equalityComparer = null) {
            if (equalityComparer != null) {
                this.comparer = equalityComparer;
            }
            else {
                this.comparer = EqualityComparer<T>.Default;
            }

            this.distinct = distinct;
            this.current = value;
        }
    }
}