﻿using System;

namespace RelayNoesisBridge.Observable.Interfaces {
    public interface IObservableReadOnlyProperty<T> {
        T Value { get; }
        event Action<T> Changed;
    }
}