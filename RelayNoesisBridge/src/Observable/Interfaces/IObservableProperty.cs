﻿namespace RelayNoesisBridge.Observable.Interfaces {
    public interface IObservableProperty<T> : IObservableReadOnlyProperty<T> {
        new T Value { get; set; }
    }
}