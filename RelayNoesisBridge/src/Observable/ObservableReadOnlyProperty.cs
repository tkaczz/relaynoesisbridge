﻿using System;
using RelayNoesisBridge.Observable.Interfaces;

namespace RelayNoesisBridge.Observable {
    public class ObservableReadOnlyProperty<T> : IObservableReadOnlyProperty<T> {
        private readonly IObservableReadOnlyProperty<T> property;

        public T Value { get { return property.Value; } }

        public event Action<T> Changed {
            add {
                property.Changed += value;
            }
            remove {
                property.Changed -= value;
            }
        }

        public ObservableReadOnlyProperty(IObservableReadOnlyProperty<T> prop) {
            this.property = prop;
        }
    }
}