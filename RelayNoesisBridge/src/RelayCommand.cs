﻿using System;
using System.Windows.Input;

namespace RelayNoesisBridge {
    public class RelayCommand : RelayCommand<object> {
        public RelayCommand(Action<object> execute) : base(execute) {
        }

        public RelayCommand(Func<object, bool> canExecute, Action<object> execute)
            : base(execute, canExecute) {
        }
    }

    //based on Noesis Sample of DelegateCommand
    //https://www.noesisengine.com/docs/Gui.Core.CommandsTutorial.html#custom-commands
    public class RelayCommand<T> : ICommand {
        private readonly Func<T, bool> canExecute;
        private readonly Action<T> execute;

        public event EventHandler CanExecuteChanged;

        public event Action<T> OnExecute;

        public bool CanExecute(object parameter) {
            return canExecute == null || canExecute((T)parameter);
        }

        public void Execute(object parameter) {
            execute((T)parameter);
            OnExecute?.Invoke((T)parameter);
        }

        public void RaiseCanExecuteChanged() {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Can execute always returns true
        /// </summary>
        public RelayCommand(Action<T> execute) {
            this.execute = execute;
        }

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute) {
            this.canExecute = canExecute;
            this.execute = execute;
        }
    }
}