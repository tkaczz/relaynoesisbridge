﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RelayNoesisBridge;
using RelayNoesisBridge.Observable;

namespace RelayNoesisBridge_Tests {
    [TestClass]
    public class RelayReadOnlyPropertyTests {
        [TestMethod]
        public void CheckIfReTransmitted() {
            var rw = new ObservableProperty<int>();
            var oro = new ObservableReadOnlyProperty<int>(rw);
            var r = new RelayReadOnlyProperty<int>(oro, "rw");

            var result = false;

            r.PropertyChanged += (sender, ev) => result = true;

            rw.Value = 42;

            Assert.IsTrue(result);
        }
    }
}