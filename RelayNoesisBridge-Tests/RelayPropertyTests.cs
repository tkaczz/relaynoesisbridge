﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RelayNoesisBridge_Tests {
    [TestClass]
    public class RelayPropertyTests {
        [TestMethod]
        public void CheckForDefaultCtorValue() {
            var prop = new RelayNoesisBridge.RelayProperty<int>("prop");

            Assert.IsTrue(prop.Value == 0);
        }

        [TestMethod]
        public void CheckIfNotDefaultValueOnCtorIfProvided() {
            var prop = new RelayNoesisBridge.RelayProperty<int>("prop", 10);

            Assert.IsTrue(prop.Value != 0);
        }

        [TestMethod]
        public void CheckForEventsOnNotDistinctSet() {
            var prop = new RelayNoesisBridge.RelayProperty<int>("prop", 10, false);
            bool result = false;

            prop.PropertyChanged += (sender, ev) => result = true;

            prop.Value = 10;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CheckForEventsOnDistinctSetIfSameValue() {
            var prop = new RelayNoesisBridge.RelayProperty<int>("prop", 10, true);
            bool result = false;

            prop.PropertyChanged += (sender, ev) => result = true;

            prop.Value = 10;

            Assert.IsTrue(!result);
        }

        [TestMethod]
        public void CheckForEventsOnDistinctSetIfNotSameValue() {
            var prop = new RelayNoesisBridge.RelayProperty<int>("prop", 10, true);
            bool result = false;

            prop.PropertyChanged += (sender, ev) => result = true;

            prop.Value = 11;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CheckForComparisonForReferenceValueIfNotSame() {
            var prop = new RelayNoesisBridge.RelayProperty<object>("prop", new object(), false);
            var snd = new object();

            prop.Value = snd;

            Assert.IsTrue(prop.Value.Equals(snd));
        }

        [TestMethod]
        public void CheckForComparisonForReferenceValueIfSame() {
            var first = new object();
            var prop = new RelayNoesisBridge.RelayProperty<object>("prop", first, true);

            var result = false;

            prop.PropertyChanged += (sender, ev) => result = true;

            prop.Value = first;

            Assert.IsTrue(!result);
        }
    }
}